## SIG-系统性能工具 
## 职责范围 Scope
- 致力于推动 OpenCloudOS 性能工具建设，包括改进、创建性能工具以及改善性能工具的生态环境。
- 推动性能领域人才的培养计划。
## SIG成员 Members
- 邢孟棒（腾讯）
- 张国强（阅码场）
## 联络方式 Contact
- 微信群
- 邮件列表
  - 小组邮件列表
    -  sig-system-performance-tool@lists.opencloudos.org
## 会议制度 Meetings
本兴趣小组的邮件列表会即时公布本小组的各项会议的具体细节。欢迎订阅我们的邮件列表。
- 例会：双周例会
- 定期沙龙： 试行月度线上沙龙
## 如何加入SIG并参与贡献：
1. 注册Gitee/GitHub账号
2. 签署CLA
3. 找到对应SIG项目仓库地址
4. 参与贡献
## SIG规划 Roadmap
- 评估现有的一些定制化性能工具是否可以开源。
- 确定性能工具建设的具体目标与实施方案。